﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JobService.Api.Helpers;
using JobService.Data.Models;
using Microsoft.Extensions.Configuration;
using SendGrid.Helpers.Mail;

namespace JobService.Api.Infrastructure
{
    public class SendGridClient
    {
        private readonly SendGrid.SendGridClient _sendGridClient;
        private readonly IConfiguration _configuration;

        public SendGridClient(IConfiguration configuration)
        {
            _configuration = configuration;
            _sendGridClient = new SendGrid.SendGridClient(_configuration["SendGrid:ApiKey"]);
        }

        public async Task SendEmailToProvidersAsync(IEnumerable<Provider> providers, Job job, List<QuoteRequest> quoteRequests)
        {
            var fromEmail = _configuration["SendGrid:FromEmail"];
            var signUp = _configuration["SignUp"];
            var randomString = RandomStringHelper.RandomString(8);
            signUp = string.Format(signUp, randomString);
            var template = _configuration["SendGrid:Template"];
            
            var subject = _configuration["SendGrid:Subject"];
            foreach (var provider in providers)
            {
                var thistemplate = template = string.Format(template, job.SenderName, job.JobCategory.ToString(), signUp,provider.CompanyEmail);

                var from = new EmailAddress(fromEmail,name:"Taask");
                var to = new EmailAddress(provider.CompanyEmail);

                if (string.IsNullOrWhiteSpace(to.Email))
                {
                    continue;
                }

                var msg = MailHelper.CreateSingleEmail(from, to, subject, string.Empty, thistemplate);
                await _sendGridClient.SendEmailAsync(msg);
                PopulateQuoteRequests(provider,job,quoteRequests,template, subject,signUp);

            }
        }

        private void PopulateQuoteRequests(Provider provider, Job job, List<QuoteRequest> quoteRequests, string body, string subject, string link)
        {
            if (quoteRequests == null)
            {
                quoteRequests = new List<QuoteRequest>();
            }

            if (quoteRequests.Any(a => a.JobId == job.Id && a.ProviderId == provider.Id))
            {
                var quoteRequest = quoteRequests.First(a => a.JobId == job.Id && a.ProviderId == provider.Id);
                quoteRequests.Remove(quoteRequest);
                quoteRequest.QuoteRequestMessages.Add(new QuoteRequestMessage()
                {
                    Type = CommunicationType.Email,
                    DateCreated = DateTime.UtcNow,
                    Id = Guid.NewGuid(),
                    Body = body,
                    Link = link,
                    Subject = subject
                });
                quoteRequests.Add(quoteRequest);
                return;

            }
            quoteRequests.Add(new QuoteRequest()
            {
                Id = Guid.NewGuid(),
                DateCreated = DateTime.UtcNow,
                JobId = job.Id,
                ProviderId = provider.Id,
                QuoteRequestMessages = new List<QuoteRequestMessage>()
                {
                    new QuoteRequestMessage()
                    {
                        Type = CommunicationType.Email,
                        DateCreated = DateTime.UtcNow,
                        Id = Guid.NewGuid(),
                        Body = body,
                        Link = link,
                        Subject = subject
                    }
                }
            });
        }
    }
}
