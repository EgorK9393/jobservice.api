﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JobService.Api.Helpers;
using JobService.Data.Models;
using Microsoft.AspNetCore.Razor.Language;
using Twilio;
using Microsoft.Extensions.Configuration;
using Twilio.Rest.Api.V2010.Account;

namespace JobService.Api.Infrastructure
{
    public class TwilioClient
    {
        private readonly IConfiguration _config;

        public TwilioClient(IConfiguration config)
        {
            _config = config;
            Twilio.TwilioClient.Init(_config["Twilio:UserName"], _config["Twilio:Password"]);
        }

        public void SendSmsToProviders(IEnumerable<Provider> providers, Job job, List<QuoteRequest> quoteRequests)
        {
            var phoneNumberFrom = _config["Twilio:PhoneNumber"];
            var template = _config["Twilio:Template"];
            var signUp = _config["SignUp"];
            var randomString = RandomStringHelper.RandomString(8);
            signUp = string.Format(signUp, randomString);
            foreach (var provider in providers)
            {
                var thistemplate = string.Format(template, job.SenderName, job.JobCategory.ToString(), signUp,provider.CompanyEmail);

                if (string.IsNullOrWhiteSpace(provider.CompanyPhone))
                {
                    continue;
                }

                var phoneNumber = provider.CompanyPhone.Replace(" ","");
                var number = "+45" + phoneNumber;

                var message = MessageResource.Create(
                        body: thistemplate,
                        from: new Twilio.Types.PhoneNumber(phoneNumberFrom),
                        to: new Twilio.Types.PhoneNumber(number)
                    );
                PopulateQuoteRequests(provider,job,quoteRequests, template, null, signUp);
            }
            
        }

        private void PopulateQuoteRequests(Provider provider, Job job, List<QuoteRequest> quoteRequests, string body, string subject, string link)
        {
            if (quoteRequests == null)
            {
                quoteRequests = new List<QuoteRequest>();
            }

            if (quoteRequests.Any(a => a.JobId == job.Id && a.ProviderId == provider.Id))
            {
                var quoteRequest = quoteRequests.First(a => a.JobId == job.Id && a.ProviderId == provider.Id);
                quoteRequests.Remove(quoteRequest);
                quoteRequest.QuoteRequestMessages.Add(new QuoteRequestMessage()
                {
                    Type = CommunicationType.Sms,
                    DateCreated = DateTime.UtcNow,
                    Id = Guid.NewGuid(),
                    Body = body,
                    Link = link,
                    Subject = subject
                });
                quoteRequests.Add(quoteRequest);
                return;

            }
            quoteRequests.Add(new QuoteRequest()
            {
                Id = Guid.NewGuid(),
                DateCreated = DateTime.UtcNow,
                JobId = job.Id,
                ProviderId = provider.Id,
                QuoteRequestMessages = new List<QuoteRequestMessage>()
                {
                    new QuoteRequestMessage()
                    {
                        Type = CommunicationType.Sms,
                        DateCreated = DateTime.UtcNow,
                        Id = Guid.NewGuid(),
                        Body = body,
                        Link = link,
                        Subject = subject
                    }
                }
            });
        }

    }
}
