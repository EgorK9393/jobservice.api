﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JobService.Data.Models;

namespace JobService.Api.Helpers
{
    public static class ZipCodeHelper
    {
        private static  List<ZipCodeZone> _zipCodeLists = new List<ZipCodeZone>()
        {
            new ZipCodeZone()
            {
                Title = "Hovedstaden",
                ZipCodeFrom = 0,
                ZipCodeTo = 3999
            },
            new ZipCodeZone()
            {
                Title = "Sjælland",
                ZipCodeFrom = 4000,
                ZipCodeTo = 4999
            },
            new ZipCodeZone()
            {
                Title = "Fyn",
                ZipCodeFrom = 5000,
                ZipCodeTo = 5999
            },
            new ZipCodeZone()
            {
                Title = "Midtjylland",
                ZipCodeFrom = 6000,
                ZipCodeTo = 7999
            },
            new ZipCodeZone()
            {
                Title = "Nordjylland",
                ZipCodeFrom = 8000,
                ZipCodeTo = 9999
            }
        };

        public static ZipCodeZone GetZipCodeZone(Job job)
        {
            var zipCodeInt = int.Parse(job.SenderZipCode);
            return _zipCodeLists.FirstOrDefault(a => a.ZipCodeFrom <= zipCodeInt && a.ZipCodeTo >= zipCodeInt);
        }
    }
}
