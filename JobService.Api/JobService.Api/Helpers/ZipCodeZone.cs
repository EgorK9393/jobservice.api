﻿namespace JobService.Api.Helpers
{
    public class ZipCodeZone
    {
        public string Title { get; set; }
        public int ZipCodeFrom { get; set; }
        public int ZipCodeTo { get; set; }
    }
}
