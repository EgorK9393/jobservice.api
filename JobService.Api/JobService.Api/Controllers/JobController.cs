﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using JobService.Api.Helpers;
using JobService.Api.Infrastructure;
using JobService.Data.Context;
using JobService.Data.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace JobService.Api.Controllers
{
    [ApiController]
    [Authorize(AuthenticationSchemes = "header")]
    public class JobController : ControllerBase
    {
        private readonly SendGridClient _sendGridClient;
        private readonly TwilioClient _twilioClient;
        private readonly JobServiceContext _context;
        private readonly IConfiguration _config;
        public JobController(SendGridClient sendGridClient, TwilioClient twilioClient, JobServiceContext context, IConfiguration config)
        {
            _sendGridClient = sendGridClient;
            _twilioClient = twilioClient;
            _context = context;
            _config = config;
        }

        [HttpPost]
        [Route("api/Job/NewleadDefault")]
        public async Task<Job> CreateJobNewleadDefault(Job job)
        {
            if (_context.GetJobByEmail(job) != null)
            {
                return new Job();
            }
            var createdJob = _context.CreateJob(job);
            var providersCount = int.Parse(_config["ProvidersCount"]);
            var germanZipCodeZone = ZipCodeHelper.GetZipCodeZone(createdJob);
            var providers = _context.GetNewleaddefaultProviders(createdJob, germanZipCodeZone?.ZipCodeFrom,germanZipCodeZone?.ZipCodeTo, providersCount);
            var quoteRequests = new List<QuoteRequest>();
            _twilioClient.SendSmsToProviders(providers, createdJob, quoteRequests);
            await _sendGridClient.SendEmailToProvidersAsync(providers,createdJob, quoteRequests);
            _context.CreateQuoteRequests(quoteRequests);
            return createdJob;
        }

        [HttpPost]
        [Route("api/Job/NewleadAll")]
        public async Task<Job> CreateJobNewleadAll(Job job)
        {
            if (_context.GetJobByEmail(job) != null)
            {
                return new Job();
            }
            var createdJob = _context.CreateJob(job);
            var providersCount = int.Parse(_config["ProvidersCount"]);
            var providers = _context.GetNewleadAllProviders(job, providersCount);
            var quoteRequests = new List<QuoteRequest>();
            _twilioClient.SendSmsToProviders(providers, createdJob, quoteRequests);
            await _sendGridClient.SendEmailToProvidersAsync(providers, job, quoteRequests);
            _context.CreateQuoteRequests(quoteRequests);
            return createdJob;
        }

        [HttpPost]
        [Route("api/Job/NewleadIgnoreRequests")]
        public async Task<Job> CreateJobNewleadIgnoreRequests(Job job)
        {
            if (_context.GetJobByEmail(job) != null)
            {
                return new Job();
            }
            var createdJob = _context.CreateJob(job);
            var providersCount = int.Parse(_config["ProvidersCount"]);
            var germanZipCodeZone = ZipCodeHelper.GetZipCodeZone(createdJob);
            var providers = _context.GetNewleadIgnoreRequestsProviders(job,germanZipCodeZone?.ZipCodeFrom, germanZipCodeZone?.ZipCodeTo, providersCount);
            var quoteRequests = new List<QuoteRequest>();
            _twilioClient.SendSmsToProviders(providers, createdJob, quoteRequests);
            await _sendGridClient.SendEmailToProvidersAsync(providers, job, quoteRequests);
            _context.CreateQuoteRequests(quoteRequests);
            return createdJob;
        }

        [HttpPost]
        [Route("api/Job/NewleadAllIgnoreRequests")]
        public async Task<Job> CreateJobNewleadAllIgnoreRequests(Job job)
        {
            if (_context.GetJobByEmail(job) != null)
            {
                return new Job();
            }
            var createdJob = _context.CreateJob(job);
            var providersCount = int.Parse(_config["ProvidersCount"]);
            var providers = _context.GetNewleadAllIgnoreRequestsProviders(job, providersCount);
            var quoteRequests = new List<QuoteRequest>();
            _twilioClient.SendSmsToProviders(providers, createdJob, quoteRequests);
            await _sendGridClient.SendEmailToProvidersAsync(providers, job, quoteRequests);
            _context.CreateQuoteRequests(quoteRequests);
            return createdJob;
        }


    }
}