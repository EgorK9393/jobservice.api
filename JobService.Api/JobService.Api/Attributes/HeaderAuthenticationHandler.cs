﻿using System;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace JobService.Api.Attributes
{
    public class HeaderAuthenticationHandler : AuthenticationHandler<HeaderAuthenticationOptions>
    {
        public IServiceProvider ServiceProvider { get; set; }

        public HeaderAuthenticationHandler(IOptionsMonitor<HeaderAuthenticationOptions> options, ILoggerFactory logger,
            UrlEncoder encoder, ISystemClock clock, IServiceProvider serviceProvider)
            : base(options, logger, encoder, clock)
        {
            ServiceProvider = serviceProvider;
        }

        protected override Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            var headers = Request.Headers;

            var config = ServiceProvider.GetService<IConfiguration>();
            var headerName = config["Authorization:HeaderName"];
            var headerValue = config["Authorization:HeaderValue"];
            var isAuthorized = false;
            if (headers.ContainsKey(headerName))
            {
                if (string.CompareOrdinal(headers[headerName], headerValue) == 0)
                {
                    isAuthorized = true;
                }
            }
            if (!isAuthorized)
            {
                return Task.FromResult(AuthenticateResult.Fail("Missing authentication header."));
            }
            var claims = new[] { new Claim("type","value") };
            var identity = new ClaimsIdentity(claims, nameof(HeaderAuthenticationHandler));
            var ticket = new AuthenticationTicket(new ClaimsPrincipal(identity), this.Scheme.Name);
            return Task.FromResult(AuthenticateResult.Success(ticket));
        }
    }
    public class HeaderAuthenticationOptions : AuthenticationSchemeOptions
    {
    }
}
