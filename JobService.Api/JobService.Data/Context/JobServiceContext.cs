﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JobService.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace JobService.Data.Context
{
    public class JobServiceContext : DbContext
    {
        public JobServiceContext(DbContextOptions options) : base(options) 
        {
            
        }

        public DbSet<Job> Jobs { get; set; }
        public DbSet<Provider> Providers { get; set; }
        public DbSet<QuoteRequest> QuoteRequests { get; set; }
        public DbSet<QuoteRequestMessage> QuoteRequestMessages { get; set; }

        public Job GetJobByEmail(Job job)
        {
            var dbJob = Jobs.FirstOrDefault(a => a.SenderEmail == job.SenderEmail);
            return dbJob;
        }

        public Job CreateJob(Job job)
        {
            job.Id = Guid.NewGuid();
            job.DateCreated = DateTime.UtcNow;
            Jobs.Add(job);
            SaveChanges();
            return job;
        }

        public List<Provider> GetNewleaddefaultProviders(Job job, int? startZipCode, int? endZipCode, int providersCount)
        {
            var providers = new List<Provider>();
            var jobCountry = job.SenderCountryIso.Replace(" ", "");
            if (startZipCode.HasValue && endZipCode.HasValue)
            {
                providers = Providers.Include(a => a.QuoteRequests).Where(a =>
                    (int)a.CompanyCategory == (int)job.JobCategory &&
                    !a.QuoteRequests.Any() && !string.IsNullOrEmpty(a.CompanyZipCode) && int.Parse(a.CompanyZipCode) >= startZipCode &&
                    int.Parse(a.CompanyZipCode) <= endZipCode && string.Compare(a.CompanyCountryIso, jobCountry,
                        StringComparison.CurrentCulture) == 0).Take(providersCount).ToList();
            }
            else
            {
                providers = Providers.Include(a => a.QuoteRequests).Where(a =>
                    (int)a.CompanyCategory == (int)job.JobCategory &&
                    !a.QuoteRequests.Any() && string.Compare(a.CompanyCountryIso, jobCountry,
                        StringComparison.CurrentCulture) == 0).ToList();
            }

            return providers;
        }

        public List<Provider> GetNewleadAllProviders(Job job, int providersCount)
        {
            var jobCountry = job.SenderCountryIso.Replace(" ", "");
            return Providers.Include(a => a.QuoteRequests).Where(a =>
                (int)a.CompanyCategory == (int)job.JobCategory &&
                !a.QuoteRequests.Any() && string.Compare(a.CompanyCountryIso, jobCountry,
                    StringComparison.CurrentCulture) == 0).Take(providersCount).ToList();
        }
        public List<Provider> GetNewleadIgnoreRequestsProviders(Job job, int? startZipCode, int? endZipCode, int providersCount)
        {
            var providers = new List<Provider>();
            var jobCountry = job.SenderCountryIso.Replace(" ", "");
            if (startZipCode.HasValue && endZipCode.HasValue)
            {
                providers = Providers.Where(a =>
                    (int)a.CompanyCategory == (int)job.JobCategory &&
                    !string.IsNullOrEmpty(a.CompanyZipCode) && int.Parse(a.CompanyZipCode) >= startZipCode &&
                    int.Parse(a.CompanyZipCode) <= endZipCode && string.Compare(a.CompanyCountryIso, jobCountry,
                        StringComparison.CurrentCulture) == 0).Take(providersCount).ToList();
            }
            else
            {
                providers = Providers.Where(a =>
                    (int)a.CompanyCategory == (int)job.JobCategory &&
                    string.Compare(a.CompanyCountryIso, jobCountry,
                        StringComparison.CurrentCulture) == 0).ToList();
            }

            return providers;
        }

        public List<Provider> GetNewleadAllIgnoreRequestsProviders(Job job, int providersCount)
        {
            var jobCountry = job.SenderCountryIso.Replace(" ", "");
            return Providers.Where(a =>
                (int)a.CompanyCategory == (int)job.JobCategory &&
               string.Compare(a.CompanyCountryIso, jobCountry,
                    StringComparison.CurrentCulture) == 0).Take(providersCount).ToList();
        }

        public void CreateQuoteRequests(List<QuoteRequest> quoteRequests)
        {
            QuoteRequests.AddRange(quoteRequests);
            SaveChanges();
        }



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new JobConfiguration());
            modelBuilder.ApplyConfiguration(new ProviderConfiguration());
            modelBuilder.ApplyConfiguration(new QuoteRequestConfiguration());
            modelBuilder.ApplyConfiguration(new QuoteRequestMessageConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}
