﻿using System;
using JobService.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace JobService.Data.Context
{
    public class JobConfiguration : IEntityTypeConfiguration<Job>
    {
        public void Configure(EntityTypeBuilder<Job> builder)
        {
            builder.ToTable("jobs");
            builder.HasKey(a => a.Id);
            builder.Property(a => a.JobCategory).HasColumnType("int");
        }
    }

    public class ProviderConfiguration : IEntityTypeConfiguration<Provider>
    {
        public void Configure(EntityTypeBuilder<Provider> builder)
        {
            builder.ToTable("providers");
            builder.HasKey(a => a.Id);
            builder.Property(a => a.CompanyCategory).HasColumnType("int");
        }
    }

    public class QuoteRequestConfiguration : IEntityTypeConfiguration<QuoteRequest>
    {
        public void Configure(EntityTypeBuilder<QuoteRequest> builder)
        {
            builder.ToTable("quote_requests");
            builder.HasKey(a => a.Id);
            builder.HasOne(a => a.Provider).WithMany(a => a.QuoteRequests).HasForeignKey(a => a.ProviderId);
            builder.HasOne(a => a.Job).WithMany(a => a.QuoteRequests).HasForeignKey(a => a.JobId);
        }
    }

    public class QuoteRequestMessageConfiguration : IEntityTypeConfiguration<QuoteRequestMessage>
    {
        public void Configure(EntityTypeBuilder<QuoteRequestMessage> builder)
        {
            builder.ToTable("quote_request_messages");
            builder.HasKey(a => a.Id);
            builder.Property(a => a.Type).HasColumnType("int");
            builder.HasOne(a => a.QuoteRequest).WithMany(a => a.QuoteRequestMessages)
                .HasForeignKey(a => a.QuoteRequestId);
        }
    }

}
