﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace JobService.Data.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "jobs",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateUpdated = table.Column<DateTime>(nullable: true),
                    DateDeleted = table.Column<DateTime>(nullable: true),
                    JobCategory = table.Column<int>(type: "int", nullable: false),
                    JobTitle = table.Column<string>(nullable: true),
                    JobDescription = table.Column<string>(nullable: true),
                    SenderName = table.Column<string>(nullable: true),
                    SenderEmail = table.Column<string>(nullable: true),
                    SenderPhone = table.Column<string>(nullable: true),
                    SenderAddress = table.Column<string>(nullable: true),
                    SenderZipCode = table.Column<string>(nullable: true),
                    SenderCity = table.Column<string>(nullable: true),
                    SenderState = table.Column<string>(nullable: true),
                    SenderCountryIso = table.Column<string>(nullable: true),
                    Source = table.Column<string>(nullable: true),
                    SourceCampaign = table.Column<string>(nullable: true),
                    SourceMedium = table.Column<string>(nullable: true),
                    SourceContent = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_jobs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "providers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    DateUpdated = table.Column<DateTime>(nullable: true),
                    DateDeleted = table.Column<DateTime>(nullable: true),
                    BlockDate = table.Column<DateTime>(nullable: true),
                    CompanyCategory = table.Column<int>(type: "int", nullable: false),
                    CompanyName = table.Column<string>(nullable: true),
                    CompanyId = table.Column<string>(nullable: true),
                    CompanyVatNumber = table.Column<string>(nullable: true),
                    CompanyAddress = table.Column<string>(nullable: true),
                    CompanyZipCode = table.Column<string>(nullable: true),
                    CompanyCity = table.Column<string>(nullable: true),
                    CompanyState = table.Column<string>(nullable: true),
                    CompanyCountryIso = table.Column<string>(nullable: true),
                    CompanyEmail = table.Column<string>(nullable: true),
                    CompanyPhone = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_providers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "quote_requests",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    JobId = table.Column<Guid>(nullable: false),
                    ProviderId = table.Column<Guid>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_quote_requests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_quote_requests_jobs_JobId",
                        column: x => x.JobId,
                        principalTable: "jobs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_quote_requests_providers_ProviderId",
                        column: x => x.ProviderId,
                        principalTable: "providers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "quote_request_messages",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: true),
                    Subject = table.Column<string>(nullable: true),
                    Body = table.Column<string>(nullable: true),
                    Type = table.Column<int>(type: "int", nullable: false),
                    Link = table.Column<string>(nullable: true),
                    QuoteRequestId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_quote_request_messages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_quote_request_messages_quote_requests_QuoteRequestId",
                        column: x => x.QuoteRequestId,
                        principalTable: "quote_requests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_quote_request_messages_QuoteRequestId",
                table: "quote_request_messages",
                column: "QuoteRequestId");

            migrationBuilder.CreateIndex(
                name: "IX_quote_requests_JobId",
                table: "quote_requests",
                column: "JobId");

            migrationBuilder.CreateIndex(
                name: "IX_quote_requests_ProviderId",
                table: "quote_requests",
                column: "ProviderId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "quote_request_messages");

            migrationBuilder.DropTable(
                name: "quote_requests");

            migrationBuilder.DropTable(
                name: "jobs");

            migrationBuilder.DropTable(
                name: "providers");
        }
    }
}
