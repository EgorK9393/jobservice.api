﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace JobService.Data.Models
{
    public class QuoteRequest
    {
        public Guid Id { get; set; }
        public Guid JobId { get; set; }
        public Guid ProviderId { get; set; }
        public DateTime? DateCreated { get; set; }
        public Job Job { get; set; }
        public Provider Provider { get; set; }
        public ICollection<QuoteRequestMessage> QuoteRequestMessages { get; set; } = new List<QuoteRequestMessage>();
    }
}
