﻿namespace JobService.Data.Models
{
    public enum CommunicationType
    {
        Email = 0,
        Sms = 1
    }
}
