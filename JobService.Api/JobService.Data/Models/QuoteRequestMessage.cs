﻿using System;

namespace JobService.Data.Models
{
    public class QuoteRequestMessage
    {
        public Guid Id { get; set; }
        public DateTime? DateCreated { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public CommunicationType Type { get; set; }
        public string Link { get; set; }
        public Guid QuoteRequestId { get; set; }
        public QuoteRequest QuoteRequest { get; set; }
    }
}
