﻿using System;
using System.Collections.Generic;

namespace JobService.Data.Models
{
    public class Provider
    {
        public Guid Id { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public DateTime? DateDeleted { get; set; }
        public DateTime? BlockDate { get; set; }
        public JobCategory CompanyCategory { get; set; }
        public string CompanyName { get; set; }
        public string CompanyId { get; set; }
        public string CompanyVatNumber { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyZipCode { get; set; }
        public string CompanyCity { get; set; }
        public string CompanyState { get; set; }
        public string CompanyCountryIso { get; set; }
        public string CompanyEmail { get; set; }
        public string CompanyPhone { get; set; }
        public ICollection<QuoteRequest> QuoteRequests { get; set; }
    }
}
