﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace JobService.Data.Models
{
    public class Job
    {
        public Guid Id { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public DateTime? DateDeleted { get; set; }
        public JobCategory JobCategory { get; set; }
        public string JobTitle { get; set; }
        public string JobDescription { get; set; }
        public string SenderName { get; set; }
        public string SenderEmail { get; set; }
        public string SenderPhone { get; set; }
        public string SenderAddress { get; set; }
        public string SenderZipCode { get; set; }
        public string SenderCity{ get; set; }
        public string SenderState { get; set; }
        public string SenderCountryIso { get; set; }
        public string Source { get; set; }
        public string SourceCampaign { get; set; }
        public string SourceMedium { get; set; }
        public string SourceContent { get; set; }
        public ICollection<QuoteRequest> QuoteRequests { get; set; }

        public Job()
        {
            SenderCountryIso = "DK";
        }
    }
}
